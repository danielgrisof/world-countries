//
//  WorldCountriesTableViewCell.swift
//  World Countries
//
//  Created by Daniel Filho on 04/05/18.
//  Copyright © 2018 E-Deploy. All rights reserved.
//

import UIKit
import SDWebImage

class WorldCountriesTableViewCell: UITableViewCell {

    // MARK: - Proprieties
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCountryCapital: UILabel!
    
    // MARK: - Life Cicle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Configure Cell
    func configureCellWith(contriesArray: [WorldCountriesModel]){
        for obj in contriesArray {
            self.lblCountryName.text = obj.countryName
            self.lblCountryCapital.text = obj.countyCapital
            self.imgCountryFlag.sd_setImage(with: URL(string: obj.countryFlag!), placeholderImage: UIImage(named: "placeHoder"), options: .cacheMemoryOnly)
        }
    }
    
}
