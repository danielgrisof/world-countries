//
//  WorldCountriesViewController.swift
//  World Countries
//
//  Created by Daniel Filho on 04/05/18.
//  Copyright © 2018 E-Deploy. All rights reserved.
//

import UIKit
import ProgressHUD

class WorldCountriesViewController: UIViewController {
    
    // MARK: - Variables
    var presenter: WorldCountriesPresenter!
    lazy var countries = [WorldCountriesModel]()
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = WorldCountriesPresenter(presenter: self)
        self.tableView.register(UINib(nibName: "WorldCountriesTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.title = "World Countries"
        self.presenter.getAllCountries()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - Class Extension For TableView
extension WorldCountriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? WorldCountriesTableViewCell {
            cell.configureCellWith(contriesArray: [countries[indexPath.row]])
            return cell
        }else{
            return WorldCountriesTableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.didSelectRowAt()
    }
}

// MARK: - Class Extension For Protocols
extension WorldCountriesViewController: WorldCountriesPresenterProtocol {
    func resultFromRequest(allCountries: [WorldCountriesModel]) {
        self.countries = allCountries
        self.tableView.reloadData()
    }
    
    
    func showAlert(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(firstButton)
        present(alertView, animated: true, completion: nil)
    }
}

