//
//  WorldCountriesPresenter.swift
//  World Countries
//
//  Created by Daniel Filho on 04/05/18.
//  Copyright © 2018 E-Deploy. All rights reserved.
//

import Foundation

protocol WorldCountriesPresenterProtocol {
    func showAlert(title: String, message: String)
    func resultFromRequest(allCountries:[WorldCountriesModel])
}

class WorldCountriesPresenter {
    
    var presenter: WorldCountriesPresenterProtocol!
    var interactor: WorldCountriesInteractor!
    
    init(presenter: WorldCountriesPresenterProtocol) {
        self.presenter = presenter
        self.interactor = WorldCountriesInteractor(interactor: self)
    }
    
    func getAllCountries(){
        self.interactor.requestData()
    }
    
    func didSelectRowAt(){
        self.presenter.showAlert(title: "Alert", message: "Worked")
    }
}

extension WorldCountriesPresenter: WorldCountriesInteractorProtocol {
    func fetchCountries(arrayCountries: [WorldCountriesModel]) {
        if arrayCountries.count < 0 {
            self.presenter.showAlert(title: "Error", message: "There are no countries in this resuqest API")
        }else{
            self.presenter.resultFromRequest(allCountries: arrayCountries)
        }
    }
}
