//
//  WorldCountriesInteractor.swift
//  World Countries
//
//  Created by Daniel Filho on 07/05/18.
//  Copyright © 2018 E-Deploy. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol WorldCountriesInteractorProtocol {
    func fetchCountries(arrayCountries: [WorldCountriesModel])
}

class WorldCountriesInteractor {
    
    var interactor: WorldCountriesInteractorProtocol!
    let url = URL(string: "https://restcountries.eu/rest/v2/all")
    
    required init(interactor: WorldCountriesInteractorProtocol) {
        self.interactor = interactor
    }
    
    func requestData(){
        guard let safeURL = url else { return }
        Alamofire.request(safeURL).responseArray { (response: DataResponse<[WorldCountriesModel]>) in
            guard let countriesArray = response.result.value else{return}
            self.interactor.fetchCountries(arrayCountries: countriesArray)
        }
    }
    
}
