//
//  WorldCountriesModel.swift
//  World Countries
//
//  Created by Daniel Filho on 04/05/18.
//  Copyright © 2018 E-Deploy. All rights reserved.
//

import Foundation
import ObjectMapper

class WorldCountriesModel: Mappable {
    
    var countryName: String?
    var countryCode: String?
    var countyCapital: String?
    var countyRegion: String?
    var countrySubRegion: String?
    var countryPopulation: Double?
    var countryCoordinate: [Double]?
    var countryArea: Double?
    var countryTimeZone: [String]?
    var countryBorders: [String]?
    var countryNativeName: String?
    var countryNumericCode: String?
    var countryCurrencies: [[String:String]]?
    var countryLanguages: [[String:String]]?
    var countryTranslation: [String:String]?
    var countryFlag: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        countryName <- map["name"]
        countryCode <- map["alpha3Code"]
        countyCapital <- map["capital"]
        countyRegion <- map["region"]
        countrySubRegion <- map["subregion"]
        countryPopulation <- map["population"]
        countryCoordinate <- map["latlng"]
        countryArea <- map["area"]
        countryTimeZone <- map["timezones"]
        countryBorders <- map["borders"]
        countryNativeName <- map["nativeName"]
        countryNumericCode <- map["numericCode"]
        countryCurrencies <- map["currencies"]
        countryLanguages <- map["languages"]
        countryTranslation <- map["translations"]
        countryFlag <- map["flag"]
    }
}
